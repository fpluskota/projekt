﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalkulator
{
    class Sub
    {
        public void sub()
        {
            Console.Write("Podaj ile liczb chcesz odjąć: ");
            int quantity = Convert.ToInt32(Console.ReadLine());
            double result = 0;
            double[] tab = new double[quantity];
            
            for (int i = 0; i < quantity; i++)
            {
                Console.Write("Podaj " + (i + 1) + " Liczbe: ");
                double number = Convert.ToDouble(Console.ReadLine());
                tab[i] = number;
                if (i == 0) result = tab[0];
                else result -= tab[i];
            }           
                Console.WriteLine("Wynik to: " + result);
        }
    }
}
 