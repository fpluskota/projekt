﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalkulator
{
    class Avg
    {
        public void avg()
        {
            Console.Write("Podaj z ilu liczb obliczyć średnią artmetyczną: ");
            int quantity = Convert.ToInt32(Console.ReadLine());
            double avg = 0;
            for (int i = 0; i < quantity; i++)
            {
                Console.Write("Podaj " + (i + 1) + " Liczbe: ");
                double number = Convert.ToDouble(Console.ReadLine());
                avg += number;
            }
            Console.WriteLine("Wynik to: " + (avg/quantity));
        }
    }
}
