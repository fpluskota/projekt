﻿using System;

namespace Kalkulator
{
    class Program
    {
        static void Main(string[] args)
        {
            int menu;
            do               
            {
                Start:
                Console.WriteLine("1. Dodawanie");
                Console.WriteLine("2. Odejmowanie");
                Console.WriteLine("3. Mnożenie");
                Console.WriteLine("4. Dzielenie");
                Console.WriteLine("5. Procent");
                Console.WriteLine("6. Średnia artmetyczna");
                Console.WriteLine("7. Potęgowanie");
                Console.WriteLine("8. Reszta z dzielenia");
                Console.WriteLine("10 Wyjście");

                try
                {
                    Console.Write("Wybierz opcje: ");
                    menu = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException exception)
                {
                    Console.WriteLine("");
                    Console.WriteLine("Musisz podać liczbe!");
                    Console.WriteLine("");
                    goto Start;
                }

                if (menu == 10)
                    break;
                if (menu < 1 || menu > 8 )
                {
                    Console.WriteLine("");
                    Console.WriteLine("Nie ma takiej opcji w menu");
                    Console.WriteLine("");
                    goto Start;
                }
                
                switch (menu)
                {
                    case 1:
                        Add addition = new Add();
                        addition.add();                      
                        break;
                    case 2:
                        Sub subtraction = new Sub();
                        subtraction.sub();
                        break;
                    case 3:
                        Mul multiplication = new Mul();
                        multiplication.mul();
                        break;
                    case 4:
                        Div division = new Div();
                        division.div();
                        break;
                    case 5:
                        Per percent = new Per();
                        percent.per();
                        break;
                    case 6:
                        Avg average = new Avg();
                        average.avg();
                        break;
                    case 7:
                        Exp exponentiation = new Exp();
                        exponentiation.exp();
                        break;
                    case 8:
                        Res_of_Div rest_of_div = new Res_of_Div();
                        rest_of_div.res();
                        break;
                }
                Console.WriteLine("");
            }            
            while (menu < 10);
        }
    }
}
